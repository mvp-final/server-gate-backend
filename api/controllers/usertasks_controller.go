package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"servergate/api/auth"
	"servergate/api/models"
	"servergate/api/utils/formaterror"

	"github.com/gin-gonic/gin"
)

func (server *Server) InsertUserTask(c *gin.Context) {
	errList = map[string]string{}

	taskID := c.Param("id")
	pid, err := strconv.ParseUint(taskID, 10, 64)
	if err != nil {
		errList["Invalid_request"] = "Invalid Request"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}
	uid, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		errList["Unauthorized"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	user := models.User{}
	err = server.DB.Debug().Model(models.User{}).Where("id = ?", uid).Take(&user).Error
	if err != nil {
		errList["Unauthorized"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	task := models.Task{}
	err = server.DB.Debug().Model(models.Task{}).Where("id = ?", pid).Take(&task).Error
	if err != nil {
		errList["Unauthorized"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}

	usertask := models.UserTask{}
	usertask.UserID = user.ID
	usertask.TaskID = task.ID

	usertaskCreated, err := usertask.SaveUserTask(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		errList = formattedError
		c.JSON(http.StatusInternalServerError, gin.H{
			"status": http.StatusInternalServerError,
			"error":  errList,
		})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"status":   http.StatusCreated,
		"response": usertaskCreated,
	})
}

func (server *Server) GetUserTask(c *gin.Context) {
	errList = map[string]string{}
	tasksID := c.Param("id")
	pid, err := strconv.ParseUint(tasksID, 10, 64)
	if err != nil {
		fmt.Println("this is the error: ", err)
		errList["Invalid_request"] = "Invalid Request"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}
	task := models.Task{}
	err = server.DB.Debug().Model(models.Task{}).Where("id = ?", pid).Take(&task).Error
	if err != nil {
		errList["No_post"] = "No Task Found"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  errList,
		})
		return
	}

	usertask := models.UserTask{}

	usertasks, err := usertask.GetUserTaskInfo(server.DB, pid)
	if err != nil {
		errList["No_tasks"] = "No tasks found"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  errList,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"response": usertasks,
	})
}

func (server *Server) DeleteUserTask(c *gin.Context) {
	usertaskID := c.Param("id")
	lid, err := strconv.ParseUint(usertaskID, 10, 64)
	if err != nil {
		errList["Invalid_request"] = "Invalid Request"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}
	uid, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		errList["Unauthorized"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	usertask := models.UserTask{}
	err = server.DB.Debug().Model(models.UserTask{}).Where("id = ?", lid).Take(&usertask).Error
	if err != nil {
		errList["No_task"] = "No task Found"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  errList,
		})
		return
	}
	if uid != usertask.UserID {
		errList["Unauthorized"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	_, err = usertask.DeleteUserTask(server.DB)
	if err != nil {
		errList["Other_error"] = "Please try again later"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  errList,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"response": "Task deleted",
	})
}
