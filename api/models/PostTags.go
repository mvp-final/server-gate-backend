package models

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

type PostTag struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	PostID    uint64    `gorm:"not null" json:"post_id"`
	TagID     uint64    `gorm:"not null" json:"tag_id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (pt *PostTag) SavePostTag(db *gorm.DB) (*PostTag, error) {
	err := db.Debug().Model(&PostTag{}).Where("tag_id = ? AND post_id = ?", pt.TagID, pt.PostID).Take(&pt).Error
	if err != nil {
		if err.Error() == "record not found" {
			err = db.Debug().Model(&PostTag{}).Create(&pt).Error
			if err != nil {
				return &PostTag{}, err
			}
		}
	} else {
		err = errors.New("double tag")
		return &PostTag{}, err
	}
	return pt, nil
}

func (pt *PostTag) DeletePostTag(db *gorm.DB) (*PostTag, error) {
	var err error
	var deletedPostTag *PostTag

	err = db.Debug().Model(PostTag{}).Where("id = ?", pt.ID).Take(&pt).Error
	if err != nil {
		return &PostTag{}, err
	} else {
		deletedPostTag = pt
		db = db.Debug().Model(&PostTag{}).Where("id = ?", pt.ID).Take(&PostTag{}).Delete(&PostTag{})
		if db.Error != nil {
			fmt.Println("cant delete PostTag: ", db.Error)
			return &PostTag{}, db.Error
		}
	}
	return deletedPostTag, nil
}

func (pt *PostTag) GetPostTagInfo(db *gorm.DB, pid uint64) (*[]PostTag, error) {

	postags := []PostTag{}
	err := db.Debug().Model(&PostTag{}).Where("tag_id = ?", pid).Find(&postags).Error
	if err != nil {
		return &[]PostTag{}, err
	}
	return &postags, err
}
