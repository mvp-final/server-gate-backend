package models

import (
	"errors"
	"fmt"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type GroupUser struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	UserID    uint32    `gorm:"not null" json:"user_id"`
	GroupID   uint64    `gorm:"not null" json:"group_id"`
	Role      string    `gorm:"text;not null;" json:"role"`
	User      User      `json:"user"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (gu *GroupUser) Prepare() {
	gu.ID = 0
	gu.Role = html.EscapeString(strings.TrimSpace(gu.Role))
	gu.User = User{}
	gu.CreatedAt = time.Now()
	gu.UpdatedAt = time.Now()
}

func (gu *GroupUser) Validate(action string) map[string]string {
	var errorMessages = make(map[string]string)
	var err error

	switch strings.ToLower(action) {
	case "update":
		if gu.Role == "" {
			err = errors.New("Required Role")
			errorMessages["Required_Role"] = err.Error()
		}
	default:
		if gu.Role == "" {
			err = errors.New("Required Role")
			errorMessages["Required_Role"] = err.Error()
		}
	}
	return errorMessages
}

func (gu *GroupUser) SaveGroupUser(db *gorm.DB) (*GroupUser, error) {
	err := db.Debug().Create(&gu).Error
	if err != nil {
		return &GroupUser{}, err
	}
	if gu.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", gu.UserID).Take(&gu.User).Error
		if err != nil {
			return &GroupUser{}, err
		}
	}
	return gu, nil
}

func (gu *GroupUser) GetGroupUsers(db *gorm.DB, pid uint64) (*[]GroupUser, error) {

	members := []GroupUser{}
	err := db.Debug().Model(&GroupUser{}).Where("group_id = ?", pid).Order("created_at desc").Find(&members).Error
	if err != nil {
		return &[]GroupUser{}, err
	}
	if len(members) > 0 {
		for i, _ := range members {
			err := db.Debug().Model(&User{}).Where("id = ?", members[i].UserID).Take(&members[i].User).Error
			if err != nil {
				return &[]GroupUser{}, err
			}
		}
	}
	return &members, err
}

func (gu *GroupUser) UpdateGroupUser(db *gorm.DB) (*GroupUser, error) {

	var err error

	err = db.Debug().Model(&GroupUser{}).Where("id = ?", gu.ID).Updates(GroupUser{Role: gu.Role, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &GroupUser{}, err
	}

	fmt.Println("this is the member role: ", gu.Role)
	if gu.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", gu.UserID).Take(&gu.User).Error
		if err != nil {
			return &GroupUser{}, err
		}
	}
	return gu, nil
}

func (gu *GroupUser) DeleteGroupUser(db *gorm.DB) (int64, error) {

	db = db.Debug().Model(&GroupUser{}).Where("id = ?", gu.ID).Take(&GroupUser{}).Delete(&GroupUser{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
