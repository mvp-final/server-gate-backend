package models

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

type UserTask struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	UserID    uint32    `gorm:"not null" json:"user_id"`
	TaskID    uint64    `gorm:"not null" json:"task_id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (utsk *UserTask) SaveUserTask(db *gorm.DB) (*UserTask, error) {
	err := db.Debug().Model(&UserTask{}).Where("task_id = ? AND user_id = ?", utsk.TaskID, utsk.UserID).Take(&utsk).Error
	if err != nil {
		if err.Error() == "record not found" {
			err = db.Debug().Model(&UserTask{}).Create(&utsk).Error
			if err != nil {
				return &UserTask{}, err
			}
		}
	} else {
		err = errors.New("double UserTask")
		return &UserTask{}, err
	}
	return utsk, nil
}

func (utsk *UserTask) DeleteUserTask(db *gorm.DB) (*UserTask, error) {
	var err error
	var deletedUserTask *UserTask

	err = db.Debug().Model(UserTask{}).Where("id = ?", utsk.ID).Take(&utsk).Error
	if err != nil {
		return &UserTask{}, err
	} else {
		deletedUserTask = utsk
		db = db.Debug().Model(&UserTask{}).Where("id = ?", utsk.ID).Take(&UserTask{}).Delete(&UserTask{})
		if db.Error != nil {
			fmt.Println("cant delete UserTask: ", db.Error)
			return &UserTask{}, db.Error
		}
	}
	return deletedUserTask, nil
}

func (utsk *UserTask) GetUserTaskInfo(db *gorm.DB, pid uint64) (*[]UserTask, error) {

	task := []UserTask{}
	err := db.Debug().Model(&UserTask{}).Where("task_id = ?", pid).Find(&task).Error
	if err != nil {
		return &[]UserTask{}, err
	}
	return &task, err
}
