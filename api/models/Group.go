package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Group struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	GroupName string    `gorm:"size:255;not null" json:"group_name"`
	GroupDesc string    `gorm:"size:255;not null" json:"group_desc"`
	Author    User      `json:"author"`
	AuthorID  uint32    `gorm:"not null" json:"author_id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (g *Group) Prepare() {
	g.GroupName = html.EscapeString(strings.TrimSpace(g.GroupName))
	g.GroupDesc = html.EscapeString(strings.TrimSpace(g.GroupDesc))
	g.Author = User{}
	g.CreatedAt = time.Now()
	g.UpdatedAt = time.Now()
}

func (g *Group) Validate() map[string]string {

	var err error

	var errorMessages = make(map[string]string)

	if g.GroupName == "" {
		err = errors.New("Required group name")
		errorMessages["Required_groupname"] = err.Error()

	}
	if g.GroupDesc == "" {
		err = errors.New("Required group desc")
		errorMessages["Required_groupdesc"] = err.Error()

	}
	if g.AuthorID < 1 {
		err = errors.New("Required Author")
		errorMessages["Required_author"] = err.Error()
	}
	return errorMessages
}

func (g *Group) SaveGroup(db *gorm.DB) (*Group, error) {
	var err error
	err = db.Debug().Model(&Group{}).Create(&g).Error
	if err != nil {
		return &Group{}, err
	}
	if g.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", g.AuthorID).Take(&g.Author).Error
		if err != nil {
			return &Group{}, err
		}
	}
	return g, nil
}

func (g *Group) FindAllGroup(db *gorm.DB) (*[]Group, error) {
	var err error
	groups := []Group{}
	err = db.Debug().Model(&Group{}).Limit(100).Order("created_at desc").Find(&groups).Error
	if err != nil {
		return &[]Group{}, err
	}
	if len(groups) > 0 {
		for t, _ := range groups {
			err := db.Debug().Model(&User{}).Where("id = ?", groups[t].AuthorID).Take(&groups[t].Author).Error
			if err != nil {
				return &[]Group{}, err
			}
		}
	}
	return &groups, nil
}

func (g *Group) FindGroupByID(db *gorm.DB, pid uint64) (*Group, error) {
	var err error
	err = db.Debug().Model(&Group{}).Where("id = ?", pid).Take(&g).Error
	if err != nil {
		return &Group{}, err
	}
	if g.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", g.AuthorID).Take(&g.Author).Error
		if err != nil {
			return &Group{}, err
		}
	}
	return g, nil
}

func (g *Group) UpdateGroup(db *gorm.DB) (*Group, error) {

	var err error

	err = db.Debug().Model(&Group{}).Where("id = ?", g.ID).Updates(Group{GroupName: g.GroupName, GroupDesc: g.GroupDesc, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Group{}, err
	}
	if g.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", g.AuthorID).Take(&g.Author).Error
		if err != nil {
			return &Group{}, err
		}
	}
	return g, nil
}

func (g *Group) DeleteGroup(db *gorm.DB) (int64, error) {

	db = db.Debug().Model(&Group{}).Where("id = ?", g.ID).Take(&Group{}).Delete(&Group{})
	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
