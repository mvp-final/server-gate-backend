package models

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

type GroupTask struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	GroupID   uint64    `gorm:"not null" json:"group_id"`
	TaskID    uint64    `gorm:"not null" json:"task_id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (gt *GroupTask) SaveGroupTask(db *gorm.DB) (*GroupTask, error) {
	err := db.Debug().Model(&GroupTask{}).Where("group_id = ? AND task_id = ?", gt.GroupID, gt.TaskID).Take(&gt).Error
	if err != nil {
		if err.Error() == "record not found" {
			err = db.Debug().Model(&GroupTask{}).Create(&gt).Error
			if err != nil {
				return &GroupTask{}, err
			}
		}
	} else {
		err = errors.New("double task")
		return &GroupTask{}, err
	}
	return gt, nil
}

func (gt *GroupTask) DeleteGroupTask(db *gorm.DB) (*GroupTask, error) {
	var err error
	var deletedGroupTask *GroupTask

	err = db.Debug().Model(GroupTask{}).Where("id = ?", gt.ID).Take(&gt).Error
	if err != nil {
		return &GroupTask{}, err
	} else {
		deletedGroupTask = gt
		db = db.Debug().Model(&GroupTask{}).Where("id = ?", gt.ID).Take(&GroupTask{}).Delete(&GroupTask{})
		if db.Error != nil {
			fmt.Println("cant delete GroupTask: ", db.Error)
			return &GroupTask{}, db.Error
		}
	}
	return deletedGroupTask, nil
}

func (gt *GroupTask) GetGroupTaskInfo(db *gorm.DB, pid uint64) (*[]GroupTask, error) {

	grouptasks := []GroupTask{}
	err := db.Debug().Model(&GroupTask{}).Where("task_id = ?", pid).Find(&grouptasks).Error
	if err != nil {
		return &[]GroupTask{}, err
	}
	return &grouptasks, err
}
