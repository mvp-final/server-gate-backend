package models

import (
	"errors"
	"fmt"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type CommentTask struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	UserID    uint32    `gorm:"not null" json:"user_id"`
	TaskID    uint64    `gorm:"not null" json:"task_id"`
	Comment   string    `gorm:"text;not null;" json:"comment"`
	User      User      `json:"user"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (ctsk *CommentTask) Prepare() {
	ctsk.ID = 0
	ctsk.Comment = html.EscapeString(strings.TrimSpace(ctsk.Comment))
	ctsk.User = User{}
	ctsk.CreatedAt = time.Now()
	ctsk.UpdatedAt = time.Now()
}

func (ctsk *CommentTask) Validate(action string) map[string]string {
	var errorMessages = make(map[string]string)
	var err error

	switch strings.ToLower(action) {
	case "update":
		if ctsk.Comment == "" {
			err = errors.New("Required Comment")
			errorMessages["Required_body"] = err.Error()
		}
	default:
		if ctsk.Comment == "" {
			err = errors.New("Required Comment")
			errorMessages["Required_body"] = err.Error()
		}
	}
	return errorMessages
}

func (ctsk *CommentTask) AddCommentTask(db *gorm.DB) (*CommentTask, error) {
	err := db.Debug().Create(&ctsk).Error
	if err != nil {
		return &CommentTask{}, err
	}
	if ctsk.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", ctsk.UserID).Take(&ctsk.User).Error
		if err != nil {
			return &CommentTask{}, err
		}
	}
	return ctsk, nil
}

func (ctsk *CommentTask) GetCommentTask(db *gorm.DB, pid uint64) (*[]CommentTask, error) {

	commentsk := []CommentTask{}
	err := db.Debug().Model(&CommentTask{}).Where("task_id = ?", pid).Order("created_at desc").Find(&commentsk).Error
	if err != nil {
		return &[]CommentTask{}, err
	}
	if len(commentsk) > 0 {
		for i, _ := range commentsk {
			err := db.Debug().Model(&User{}).Where("id = ?", commentsk[i].UserID).Take(&commentsk[i].User).Error
			if err != nil {
				return &[]CommentTask{}, err
			}
		}
	}
	return &commentsk, err
}

func (ctsk *CommentTask) UpdateCommentTask(db *gorm.DB) (*CommentTask, error) {

	var err error

	err = db.Debug().Model(&CommentTask{}).Where("id = ?", ctsk.ID).Updates(CommentTask{Comment: ctsk.Comment, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &CommentTask{}, err
	}

	fmt.Println("this is the comment Comment: ", ctsk.Comment)
	if ctsk.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", ctsk.UserID).Take(&ctsk.User).Error
		if err != nil {
			return &CommentTask{}, err
		}
	}
	return ctsk, nil
}

func (ctsk *CommentTask) DeleteCommentTask(db *gorm.DB) (int64, error) {

	db = db.Debug().Model(&CommentTask{}).Where("id = ?", ctsk.ID).Take(&CommentTask{}).Delete(&CommentTask{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
