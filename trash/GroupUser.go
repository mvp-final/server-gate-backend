package models

import (
	"errors"
	"fmt"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type GroupUser struct { //member
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	GroupID   uint64    `gorm:"not null" json:"group_id"`
	UserID    uint32    `gorm:"not null" json:"user_id"`
	UserRole  string    `gorm:"text;not null;" json:"user_role"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (gu *GroupUser) Prepare() {
	gu.ID = 0
	gu.UserRole = html.EscapeString(strings.TrimSpace(gu.UserRole))
	gu.CreatedAt = time.Now()
	gu.UpdatedAt = time.Now()
}

func (gu *GroupUser) Validate(action string) map[string]string {
	var errorMessages = make(map[string]string)
	var err error

	switch strings.ToLower(action) {
	case "update":
		if gu.UserRole == "" {
			err = errors.New("Required UserRole")
			errorMessages["Required_UserRole"] = err.Error()
		}
	default:
		if gu.UserRole == "" {
			err = errors.New("Required UserRole")
			errorMessages["Required_UserRole"] = err.Error()
		}
	}
	return errorMessages
}

func (gu *GroupUser) SaveGroupUser(db *gorm.DB) (*GroupUser, error) {
	err := db.Debug().Model(&GroupUser{}).Where("group_id = ? AND user_id = ?", gu.GroupID, gu.UserID).Take(&gu).Error
	if err != nil {
		if err.Error() == "record not found" {
			err = db.Debug().Model(&GroupUser{}).Create(&gu).Error
			if err != nil {
				return &GroupUser{}, err
			}
		}
	} else {
		err = errors.New("double member")
		return &GroupUser{}, err
	}
	return gu, nil
}

func (gu *GroupUser) DeleteGroupUser(db *gorm.DB) (*GroupUser, error) {
	var err error
	var deletedMember *GroupUser

	err = db.Debug().Model(GroupUser{}).Where("id = ?", gu.ID).Take(&gu).Error
	if err != nil {
		return &GroupUser{}, err
	} else {
		deletedMember = gu
		db = db.Debug().Model(&GroupUser{}).Where("id = ?", gu.ID).Take(&GroupUser{}).Delete(&GroupUser{})
		if db.Error != nil {
			fmt.Println("cant delete member: ", db.Error)
			return &GroupUser{}, db.Error
		}
	}
	return deletedMember, nil
}

func (gu *GroupUser) GetGroupUserInfo(db *gorm.DB, pid uint64) (*[]GroupUser, error) {

	members := []GroupUser{}
	err := db.Debug().Model(&GroupUser{}).Where("user_id = ?", pid).Find(&members).Error
	if err != nil {
		return &[]GroupUser{}, err
	}
	return &members, err
}

func (gu *GroupUser) DeleteGroupMember(db *gorm.DB, uid uint32) (int64, error) {
	members := []GroupUser{}
	db = db.Debug().Model(&GroupUser{}).Where("user_id = ?", uid).Find(&members).Delete(&members)
	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}

// func (gu *GroupUser) DeletePostLikes(db *gorm.DB, pid uint64) (int64, error) {
// 	likes := []GroupUser{}
// 	db = db.Debug().Model(&GroupUser{}).Where("post_id = ?", pid).Find(&likes).Delete(&likes)
// 	if db.Error != nil {
// 		return 0, db.Error
// 	}
// 	return db.RowsAffected, nil
// }
